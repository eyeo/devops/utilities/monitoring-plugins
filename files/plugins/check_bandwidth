#!/usr/bin/python3

import os, re, subprocess, sys, socket, struct, fcntl

INTERVAL = 5

def format_bandwidth(bits):
  if bits >= 1000000:
    return "%.2f Mbit/s" % (bits / 1000000)
  elif bits >= 1000:
    return "%.2f kbit/s" % (bits / 1000)
  else:
    return "%.2f bit/s" % bits

def getmacaddress(nic="eth0"):
  # See man netdevice for the request structure: it has to start with 16 bytes
  # containing the interface name, the OS will write 8 bytes after that (2 bytes
  # family name and 6 bytes actual MAC address).
  s = socket.socket()
  SIOCGIFHWADDR = 0x8927 # see man ioctl_list

  return fcntl.ioctl(s.fileno(), SIOCGIFHWADDR, struct.pack('24s',
                       bytes([ord(char) for char in nic])))[18:24]

def list_if():
  nics = []
  pattern = re.compile(r'^\d+:\s*')
  output = os.popen('ip addr show').read().rstrip().split('\n')

  for line in output:
    if re.match(pattern, line):
      fields = line.split(": ")
      nics.append(fields[1])

  return nics

if __name__ == "__main__":
  if len(sys.argv) != 5:
    script_name = os.path.basename(sys.argv[0])
    print("Usage: %s -w WARN -c CRIT" % script_name)
    sys.exit(0)

  warn = int(sys.argv[2])
  crit = int(sys.argv[4])
  nics = list_if()
  exit_code = 0
  output = ''

  for nic in nics:
    process = subprocess.Popen(
      ["sudo", "tcpdump", "-q", "-s", "64", "-G", str(INTERVAL), "-W", "1", "-w", "-"],
      stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    mac = getmacaddress(nic)

    total = {"rx": 0, "tx": 0}
    http = {"rx": 0, "tx": 0}
    https = {"rx": 0, "tx": 0}
    ssh = {"rx": 0, "tx": 0}
    dns = {"rx": 0, "tx": 0}
    other = {"rx": 0, "tx": 0}
    other_detailed = {}

    # See http://wiki.wireshark.org/Development/LibpcapFileFormat for libpcap format description
    global_header = process.stdout.read(24)
    magic_number, _, _, _, _, _, _ = struct.unpack("IHHiIII", global_header)

    if magic_number != 0xa1b2c3d4:
      raise Exception("Unexpected format")

    while True:
      record_header = process.stdout.read(16)
      if record_header == b'':
        break;
      _, _, incl_len, orig_len = struct.unpack("IIII", record_header)

      # Convert bytes to bits and normalize to seconds
      bps = float(orig_len * 8) / INTERVAL

      def add_other(description):
        other[direction] += bps
        other_detailed[description] = other_detailed.get(description, 0) + bps

      payload = process.stdout.read(incl_len)

      # Unpack Ethernet frame
      # http://en.wikipedia.org/wiki/Ethernet_frame#Structure
      # Note that tcpdump doesn't capture the preamble,
      # start of frame delimiter and the interframe gap, these are handled
      # internally by the network card.
      destination, source, protocol = struct.unpack("!6s6sH", payload[:14])
      payload = payload[14:]
      direction = "rx" if destination == mac else "tx"
      total[direction] += bps

      # Check Level 3 protocol
      # IPv4, http://en.wikipedia.org/wiki/Internet_Protocol_version_4#Header
      if protocol == 0x0800:
        ihl = payload[0] & 0xF
        protocol = payload[9]
        payload = payload[ihl * 4:]
      # IPv6, http://en.wikipedia.org/wiki/IPv6_packet#Fixed_header
      elif protocol == 0x86DD:
        protocol = payload[6]
        payload = payload[40:]
      else:
        add_other('L3 0x{0:04X}'.format(protocol))
        continue

      # Check Level 4 protocol
      if protocol in (0x06, 0x11):    # TCP, UDP
        source_port, destination_port = struct.unpack('!HH', payload[:4])
        protocol = "TCP" if protocol == 0x06 else "UDP"

        # The lower port number should be the real port, the other one will be
        # the ephemeral port.
        port = min(source_port, destination_port)
      else:
        add_other('L4 0x{0:02X}'.format(protocol))
        continue

      if protocol == "TCP" and port == 80:
        http[direction] += bps
      elif protocol == "TCP" and port == 443:
        https[direction] += bps
      elif protocol == "TCP" and port == 22:
        ssh[direction] += bps
      elif port == 53:
        dns[direction] += bps
      else:
        add_other('Port {}'.format(port))
        continue

    status = []

    def add_status(_id, values):
      rx = values["rx"]
      tx = values["tx"]
      status.append('{}rx {} {}tx {}'.format(_id, format_bandwidth(rx), _id,
                                             format_bandwidth(tx)))

    add_status("total_", total)
    add_status("http_", http)
    add_status("https_", https)
    add_status("ssh_", ssh)
    add_status("dns_", dns)
    add_status("other_", other)

    for key in sorted(iter(other_detailed.keys()),
                      key=lambda k: other_detailed[k],
                      reverse=True):
      status.append(
        '{} {}'.format(
          key,
          format_bandwidth(float(other_detailed[key]) / INTERVAL)),
      )

    status_string = '\n'.join(status)
    output = '{}\n{}:\n{}\n'.format(output, nic, status_string)

    if total["rx"] >= crit or total["tx"] >= crit:
      exit_status = 2
    elif (total["rx"] >= warn or total["tx"] >= warn) and exit_code != 2:
      exit_status = 1

  if exit_code == 2:
    output = "BANDWIDTH CRITICAL" + output.rstrip()
  elif exit_code == 1:
    output = "BANDWIDTH WARNING" + output.rstrip()
  else:
    output = "BANDWIDTH OK" + output.rstrip()

  print(output)
  sys.exit(exit_code)
